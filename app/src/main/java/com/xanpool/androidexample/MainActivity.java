package com.xanpool.androidexample;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.appboxo.js.params.CustomEvent;
import com.appboxo.log.DefaultLogger;
import com.appboxo.sdk.Appboxo;
import com.appboxo.sdk.Config;
import com.appboxo.sdk.MiniApp;

import kotlin.Unit;
import kotlin.jvm.functions.Function3;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Appboxo.INSTANCE.init(this)
                .setConfig(new Config.Builder()
                        .setClientId("784198")
                        .build())
                .setLogger(new DefaultLogger(BuildConfig.DEBUG));
    }

    public void onTopUp(View view) {
        MiniApp app = Appboxo.INSTANCE.createMiniApp("app54027", "{'transactionType': 'buy'}");
        app.open(this);
    }

    public void onWithdraw(View view) {
        MiniApp app = Appboxo.INSTANCE.createMiniApp("app54027", "{'transactionType': 'sell'}");
        app.open(this);
    }

    // User doesn't have to enter his deposit wallet
    // Exchange/wallet can pre-define their deposit wallets
    public void onTopUpWithWallet(View view) {
        MiniApp app = Appboxo.INSTANCE.createMiniApp("app54027", "{'transactionType': 'buy', 'wallet': '1MLaPXe8ZtBB4Bpud3JmpzAoNWHgy4JV4w', 'cryptoCurrency': 'btc'}");
        app.open(this);
    }

    // User doesn't have to transfer bitcoins for selling
    // Exchange/wallet will transfer it on user's behalf
    public void onAutomaticWithdraw(View view) {
        MiniApp app = Appboxo.INSTANCE.createMiniApp("app54027", "{'transactionType': 'sell', 'autoSelling': 'true'}");
        app.setCustomEventListener(new Function3<Activity, MiniApp, CustomEvent, Unit>() {
            @Override
            public Unit invoke(Activity activity, MiniApp miniApp, CustomEvent customEvent) {
                Toast toast = Toast.makeText(activity, customEvent.type, 3);
                toast.show();
                return null;
            }
        });
        app.open(this);
    }
}
