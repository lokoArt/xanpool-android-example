# xanpool-android-example
An example of integration with XanPool Widget through AppBoxo.

## Description
Here is a snipped of the code from MainActiviy.java needed for XanPool integration

**Generic top up**
```
public void onTopUp(View view) {
    MiniApp app = Appboxo.INSTANCE.createMiniApp("app54027", "{'transactionType': 'buy'}");
    app.open(this);
}
```

**Generic Withdraw**
```
public void onWithdraw(View view) {
    MiniApp app = Appboxo.INSTANCE.createMiniApp("app54027", "{'transactionType': 'sell'}");
    app.open(this);
}
```

**Top up with pre-defined wallet**  
User doesn't have to enter his deposit wallet, Exchange/wallet can pre-defined their deposit wallets
```
public void onTopUpWithWallet(View view) {
    MiniApp app = Appboxo.INSTANCE.createMiniApp("app54027", "{'transactionType': 'buy', 'wallet': '1MLaPXe8ZtBB4Bpud3JmpzAoNWHgy4JV4w', 'cryptoCurrency': 'btc'}");
    app.open(this);
}
```

**Automated selling**  
Exchange/wallet transfers crypto for selling on user's behalf
```
public void onAutomaticWithdraw(View view) {
    MiniApp app = Appboxo.INSTANCE.createMiniApp("app54027", "{'transactionType': 'sell', 'autoSelling': 'true'}");
    // Adding a listener to be notified once a transaction has been initiated by the user
    app.setCustomEventListener(new Function3<Activity, MiniApp, CustomEvent, Unit>() {
        @Override
        public Unit invoke(Activity activity, MiniApp miniApp, CustomEvent customEvent) {
            Toast toast = Toast.makeText(activity, customEvent.type, 3);
            toast.show();
            return null;
        }
    });
    app.open(this);
}
```